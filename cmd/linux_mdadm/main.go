//
// main.go
// Copyright (C) 2017 kevin <kevin@phrye.com>
//
// Distributed under terms of the GPL license.
//

package main

import (
	"fmt"
	"gitlab.com/sysinfo/raid/mdadm"
)

func main() {
	raids, err := mdadm.GetMdDevices("/proc/diskstats")
	if err != nil {
		fmt.Errorf("Error getting mdadm info (%s)\n", err)
		return
	}
	fmt.Printf("RAID device count: %d\n", len(raids))
	for _, raid := range raids {
		fmt.Printf("Info for %+p\n", raid)
	}
}
